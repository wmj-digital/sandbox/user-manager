<?php

namespace Wmj\UserManager\Tests\Unit\ObjectCache;

use Iterator;
use PHPUnit\Framework\MockObject\MockObject;
use stdClass;
use Throwable;
use PHPUnit\Framework\TestCase;
use Wmj\UserManager\ObjectCache\ObjectCacheTrait;

class ObjectCacheTraitTest extends TestCase
{
    /**
     * @var MockObject | ObjectCacheTrait
     */
    private MockObject $cache;

    function setUp(): void
    {
        $this->cache = $this->getMockForTrait(ObjectCacheTrait::class);
    }

    function test_return_null_when_object_never_been_cached(): void
    {
        $this->assertNull($this->cache->getFromCache('fghjhtrertghj'));
    }

    function test_keep_object_when_no_external_reference_exist():void{

        $this->cache->addToCache(new stdClass());
        $this->assertInstanceOf(stdClass::class,  $this->cache->getFromCache(stdClass::class));
    }

    function test_when_no_id_set_user_class_name_as_id(): void
    {
        $cls = new stdClass();
        $this->cache->addToCache($cls);
        $this->assertSame($cls, $this->cache->getFromCache(get_class($cls)));
    }

    function test_when_id_set_use_it(): void
    {
        $cls = new stdClass();
        $id = 'id';
        $this->cache->addToCache($cls, $id);
        $this->assertSame($cls, $this->cache->getFromCache($id));
    }

    /**
     * @param $id
     *
     * @dataProvider correctId
     */
    function test_accept_correct_index($id): void
    {
        $this->cache->addToCache(new stdClass(), $id);
        $this->assertNotNull($this->cache->getFromCache($id));
    }

    function correctId(): Iterator
    {
        yield ['id'];
        yield ['id with spaces'];
        yield [3];
    }

    /**
     * @param $incorrectId
     *
     * @dataProvider incorrectId
     */
    function test_exception_if_index_is_not_integer_or_string_or_null($incorrectId): void
    {
        $this->expectException(Throwable::class);
        $this->cache->addToCache(new stdClass(), $incorrectId);
    }

    function incorrectId(): Iterator
    {
        yield [-4];
        yield [1.1];
        yield [true];
        yield [function () {
        }];
        yield [new stdClass()];
        yield [''];
    }
}
