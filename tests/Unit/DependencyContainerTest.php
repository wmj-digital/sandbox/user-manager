<?php

namespace Wmj\UserManager\Tests\Unit;

use Iterator;
use PHPUnit\Framework\MockObject\MockObject;
use ReflectionException;
use Wmj\UserManager\Assets\Configuration;
use Wmj\UserManager\DependencyContainer;
use PHPUnit\Framework\TestCase;
use Wmj\UserManager\DependencyNotFoundException;
use Wmj\UserManager\Tests\AccessibleProperty;

class DependencyContainerTest extends TestCase
{
    const P_DEPENDENCY_CREATORS = 'dependencyCreators';

    /**
     * @var DependencyContainer | MockObject
     */
    protected MockObject $container;

    /**
     * @param string $class
     *
     * @dataProvider declaredClass
     */
    function test_container_can_return_all_declared_dependency(string $class): void
    {
        $configMock = $this->createMock(Configuration::class);
        $this->container = $this->getMockBuilder(DependencyContainer::class)->setConstructorArgs([$configMock])->onlyMethods(['getPdo'])->getMock();
        $pdoMock = $this->createMock(\PDO::class);
        $pdoMock->method('prepare')->willReturn($this->createMock(\PDOStatement::class));
        $this->container->method('getPdo')->willReturn($pdoMock);

        $this->assertInstanceOf($class, $this->container->get($class));
    }

    /**
     * @param string $class
     *
     * @dataProvider declaredClass
     */
    function test_container_confirm_ownership_of_all_declared_class(string $class): void
    {
        $container = $this->createContainer();
        $this->assertTrue($container->has($class));
    }

    /**
     * @return DependencyContainer
     */
    function createContainer(): DependencyContainer
    {
        $configMock = $this->createMock(Configuration::class);
        return new DependencyContainer($configMock);
    }

    /**
     *
     */
    function test_container_not_confirm_ownershop_of_not_declared_class(): void
    {
        $container = $this->createContainer();
        $this->assertFalse($container->has('randomClass'));
    }

    /**
     * @return Iterator
     * @throws ReflectionException
     */
    function declaredClass(): Iterator
    {
        $container = $this->createContainer();
        $creators = new AccessibleProperty($container, self::P_DEPENDENCY_CREATORS);
        foreach (array_keys($creators->get()) as $class) {
            yield [$class];
        }
    }

    function test_container_throw_exception_when_try_get_not_declared_class(): void
    {
        $container = $this->createContainer();
        $this->expectException(DependencyNotFoundException::class);
        $container->get('randomTestClass');
    }
}
