<?php

namespace Wmj\UserManager\Tests\Unit\Model;

use PHPUnit\Framework\MockObject\MockObject;
use ReflectionException;
use Wmj\UserManager\Model\EntryIndex;
use Wmj\UserManager\Model\IndexExistCheckVisitor;
use PHPUnit\Framework\TestCase;
use Wmj\UserManager\Tests\AccessibleProperty;

class IndexExistCheckVisitorTest extends TestCase
{
    /**
     * @var IndexExistCheckVisitor
     */
    private IndexExistCheckVisitor $checker;
    /**
     * @var AccessibleProperty
     */
    private AccessibleProperty $checkCallback;
    /**
     * @var MockObject | EntryIndex
     */
    private MockObject $entryIndexMock;

    /**
     * @throws ReflectionException
     */
    function setUp(): void
    {
        $this->checker = new IndexExistCheckVisitor();
//        $this->checkCallback = new AccessibleProperty($this->checker, '$indexChecker');
        $this->entryIndexMock = $this->getMockBuilder(EntryIndex::class)->disableOriginalConstructor()->getMockForAbstractClass();
    }

    function test_call_callback_with_object_arg():void{
        $this->checker->setIndexCheckerCallback(fn($mockObject) => $this->assertInstanceOf(MockObject::class, $mockObject));
        $this->checker->inStorage($this->entryIndexMock);
    }
}
