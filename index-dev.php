<?php declare(strict_types=1);

use Wmj\UserManager\App;

error_reporting(E_ALL);

require_once(__DIR__ . '/vendor/autoload.php');

App::instance()->run();
