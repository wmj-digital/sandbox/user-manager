<?php


namespace Wmj\UserManager;

/**
 * Interface PrototypeInterface
 *
 * Read only prototype cannot be mutated and should have factory methods returning its editable clone.
 * Only prototype should allow cloning.
 *
 * @package Wmj\UserManager
 */
interface PrototypeInterface
{
    /**
     * @return bool
     */
    function isPrototype(): bool;

    /**
     * @return $this
     */
    function markAsPrototype(): self;
//
//    /**
//     * @return $this
//     */
//    function clone(): self; //Maybe will be remove in future versions.
}