<?php


namespace Wmj\UserManager;


use PDO;
use Psr\Container\ContainerInterface;
use Wmj\UserManager\Adapter\RestApi\UserGroups\GroupCollectionEndpoint;
use Wmj\UserManager\Adapter\RestApi\UserGroups\GroupEndpoint;
use Wmj\UserManager\Adapter\RestApi\UserGroups\GroupMembersEndpoint;
use Wmj\UserManager\Adapter\RestApi\Users\UserEndpoint;
use Wmj\UserManager\Adapter\RestApi\Users\UserGroupEndpoint;
use Wmj\UserManager\Adapter\RestApi\Users\UsersCollectionEndpoint;
use Wmj\UserManager\Assets\Configuration;
use Wmj\UserManager\Adapter\DefaultEndpoint;
use Wmj\UserManager\Adapter\RootEndpoint;
use Wmj\UserManager\Adapter\Router;
use Wmj\UserManager\Model\StorageService;
use Wmj\UserManager\Model\User\UserBuilder;
use Wmj\UserManager\Model\User\UserFactory;
use Wmj\UserManager\Model\User\UserRepository;
use Wmj\UserManager\Model\UserGroup\UserGroupFactory;

/**
 * Class DependencyContainer
 *
 * @package Wmj\UserManager
 */
class DependencyContainer implements ContainerInterface
{
    /**
     * @var array<string, object>
     */
    protected array $cache = [];
    /**
     * @var array<string, callable>
     */
    protected array $dependencyCreators = [];

    /**
     * DependencyContainer constructor.
     *
     * @param Configuration $configuration
     */
    function __construct(Configuration $configuration)
    {
        $this->dependencyCreators = [
            Configuration::class => fn(): Configuration => $configuration,
            PDO::class => fn(): PDO => $this->getPdo(),
            Router::class => fn(): Router => new Router(),
            DefaultEndpoint::class => fn(): DefaultEndpoint => new DefaultEndpoint(),
            RootEndpoint::class => fn(): RootEndpoint => new RootEndpoint(),
            UsersCollectionEndpoint::class => fn(): UsersCollectionEndpoint => new UsersCollectionEndpoint($this->getUserFactory()->makeUserBuilder(), $this->getUserFactory()->getUserRepository()),
            UserEndpoint::class => fn(): UserEndpoint => new UserEndpoint($this->getUserFactory()->makeUserBuilder(), $this->getUserFactory()->getUserRepository()),
            GroupEndpoint::class => fn(): GroupEndpoint => new GroupEndpoint($this->getGroupFactory()->getGroupRepository(), $this->getGroupFactory()->makeGroupBuilder()),
            GroupMembersEndpoint::class => fn(): GroupMembersEndpoint => new GroupMembersEndpoint($this->getGroupFactory()->getGroupRepository(), $this->getGroupFactory()->makeGroupBuilder()),
            GroupCollectionEndpoint::class => fn(): GroupCollectionEndpoint => new GroupCollectionEndpoint($this->getGroupFactory()->getGroupRepository(), $this->getGroupFactory()->makeGroupBuilder()),
            UserBuilder::class => fn(): UserBuilder => $this->getUserFactory()->makeUserBuilder(),
            UserRepository::class => fn(): UserRepository => $this->getUserFactory()->getUserRepository(),
            UserGroupFactory::class => fn(): UserGroupFactory => $this->getGroupFactory(),
            UserGroupEndpoint::class => fn(): UserGroupEndpoint => new UserGroupEndpoint($this->getGroupFactory()->getGroupRepository(), $this->getUserFactory()->makeUserBuilder())
        ];
    }

    /**
     * @return PDO
     */
    protected function getPdo(): PDO
    {
        if (!array_key_exists(PDO::class, $this->cache)) {
            $config = $this->getConfiguration();
            $this->cache[PDO::class] = new PDO($config->getDbConnectString(), $config->getDbUser(), $config->getDbPassword());
        }

        return $this->cache[PDO::class];
    }

    /**
     * @return Configuration
     */
    protected function getConfiguration(): Configuration
    {
        return $this->dependencyCreators[Configuration::class]();
    }

    /**
     * @return UserFactory
     */
    protected function getUserFactory(): UserFactory
    {
        if ($this->missInCache(UserFactory::class)) {
            $this->cache[UserFactory::class] = new UserFactory($this->getPdo() , $this->getStorageProvider());
        }

        return $this->cache[UserFactory::class];
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    protected function missInCache(string $id): bool
    {
        return !array_key_exists($id, $this->cache);
    }

    /**
     * @return StorageService
     */
    protected function getStorageProvider(): StorageService
    {
        if ($this->missInCache(StorageService::class)) {
            $this->cache[StorageService::class] = new StorageService($this->getPdo());
        }
        return $this->cache[StorageService::class];
    }

    /**
     * @return UserGroupFactory
     */
    protected function getGroupFactory(): UserGroupFactory
    {
        if ($this->missInCache(UserGroupFactory::class)) {
            $this->cache[UserGroupFactory::class] = new UserGroupFactory($this->getPdo(), $this->getStorageProvider());
        }

        return $this->cache[UserGroupFactory::class];
    }

    /**
     * @param string $id
     *
     * @return mixed
     */
    function get(string $id)
    {
        if (!$this->has($id)) throw new DependencyNotFoundException();
        return $this->dependencyCreators[$id]();
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    function has(string $id): bool
    {
        return array_key_exists($id, $this->dependencyCreators);
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    protected function inCache(string $id): bool
    {
        return array_key_exists($id, $this->cache);
    }
}