<?php


namespace Wmj\UserManager\Model;


use PDO;
use PDOException;
use PDOStatement;
use RuntimeException;

class StorageService
{
    protected PDO $pdo;
    private bool $transactionStarted = false;

    function __construct(PDO $PDO)
    {
        $this->pdo = $PDO;
    }

    function executeStatement(PDOStatement $statement): PDOStatement
    {
        try {
            $statement->execute();
        } catch (PDOException $exception) {
            $this->rejectTransaction();
            throw new RuntimeException( implode(' ',$statement->errorInfo()));
        }

        return $statement;
    }

    function getLastInsertedId(){
        return $this->pdo->lastInsertId();
    }

    function beginTransaction(): void
    {
        if (!$this->transactionStarted) {
            $this->pdo->beginTransaction();
            $this->transactionStarted = true;
        }

    }

    function commitTransaction(): void
    {
        $this->endTransactoin(fn() => $this->pdo->commit());
    }

    private function endTransactoin(callable $transactionEnd): void
    {
        if ($this->transactionStarted) {
            $transactionEnd();
            $this->transactionStarted = false;
        }
    }

    function rejectTransaction(): void
    {
        $this->endTransactoin(fn() => $this->pdo->rollBack());

    }
}