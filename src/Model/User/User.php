<?php


namespace Wmj\UserManager\Model\User;


use Closure;
use Wmj\UserManager\Model\AbstractEntity;
use Wmj\UserManager\Model\EntryIndex;
use Wmj\UserManager\Model\User\Validation\BirthdayMustBeInIsoFormat;
use Wmj\UserManager\Model\User\Validation\UserNameCannotBeEmptyException;

/**
 * Class User
 *
 * @package Wmj\UserManager\Model\User
 */
class User extends AbstractEntity
{

    const NAME = 'name';
    const PASSWORD = 'password';
    const FIRST_NAME = 'first_name';
    const LAST_NAME = 'last_name';
    const BIRTHDAY = 'birthday';
    /**
     * @var EntryIndex
     */
    private EntryIndex $index;

    /**
     * User constructor.
     *
     * @param EntryIndex $dbIndex
     */
    function __construct(EntryIndex $dbIndex)
    {
        $this->index = $dbIndex;
    }

    /**
     * @return EntryIndex
     */
    function getIndex(): EntryIndex
    {
        return $this->index;
    }

    function setIndex(UserIndex $index):self{
        $this->index = $index;

        return $this;
    }

    /**
     * @return Closure[]
     */
    protected function getSimpleValueNameProcessorPairs(): array
    {
        return [
            self::NAME => function (string $name): string {
                if(empty($name)) throw new UserNameCannotBeEmptyException();
                return $name;
            },
            self::PASSWORD => function (string $password): string {
                //todo: create separate class Password instead simple value | add methods setHash, setPassword, verify($password):bool, getHash(), setPepper(string $pepper):void
                //todo check which password is smaller or equal to 72 character | bcrypt algo truncate password to 72 character
                return password_hash($password,PASSWORD_BCRYPT);
            },
            self::FIRST_NAME => function (string $firstname): string {
                return $firstname;
            },
            self::LAST_NAME => function (string $lastName): string {
                return $lastName;
            },
            self::BIRTHDAY => function (string $birthday): string {
                if (empty($birthday)) throw new BirthdayMustBeInIsoFormat('Empty string given.');
                return $birthday;
            }
        ];
    }

    /**
     * @return EntryIndex[]
     */
    protected function getComplexValues(): array
    {
        return [
            $this->index::INDEX_NAME => $this->index
        ];
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    function setName(string $name):self{
        $this->offsetSet(self::NAME, $name);
        return $this;
    }

    /**
     * @return string|null
     */
    function getName():?string{
        return $this->offsetGet(self::NAME);
    }
}