<?php


namespace Wmj\UserManager\Model\User\Validation;


/**
 * Class UserNameAlreadyExistException
 *
 * @package Wmj\UserManager\Model\User\Validation
 */
class UserNameAlreadyExistException extends UserValidationException
{
}