<?php


namespace Wmj\UserManager\Model\User\Validation;


/**
 * Class UserNameCannotBeEmptyException
 *
 * @package Wmj\UserManager\Model\User\Validation
 */
class UserNameCannotBeEmptyException extends UserValidationException
{

}