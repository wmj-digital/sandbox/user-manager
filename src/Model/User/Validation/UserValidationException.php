<?php


namespace Wmj\UserManager\Model\User\Validation;


use Wmj\UserManager\Model\ValidationException;

/**
 * Class UserValidationException
 *
 * @package Wmj\UserManager\Model\User\Validation
 */
class UserValidationException extends ValidationException
{

}