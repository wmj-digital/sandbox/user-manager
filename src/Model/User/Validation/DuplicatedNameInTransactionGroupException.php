<?php


namespace Wmj\UserManager\Model\User\Validation;


/**
 * Class DuplicatedNameInTransactionGroupException
 *
 * @package Wmj\UserManager\Model\User\Validation
 */
class DuplicatedNameInTransactionGroupException extends UserValidationException
{

}