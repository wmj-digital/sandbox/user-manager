<?php


namespace Wmj\UserManager\Model\User;


use PDO;
use PDOStatement;
use Wmj\UserManager\Model\EntryIndex;
use Wmj\UserManager\Model\MySqlStatementStringGeneratorTrait;

/**
 * Class MysqlUserStatementCreator
 *
 * @package Wmj\UserManager\Model\User
 */
class MysqlUserStatementBuilder
{
    use MySqlStatementStringGeneratorTrait;

    const TABLE_NAME = UserRepository::COLLECTION_NAME;
    const COLUMN_INDEX = EntryIndex::INDEX_LABEL;
    const COLUMN_NAME = User::NAME;
    const COLUMN_PASSWORD = User::PASSWORD;
    const COLUMN_FIRST_NAME = User::FIRST_NAME;
    const COLUMN_LAST_NAME = User::LAST_NAME;
    const COLUMN_BIRTHDAY = User::BIRTHDAY;

    protected PDO $pdo;

    /**
     * MysqlUserStatementCreator constructor.
     *
     * @param PDO $pdo
     */
    function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @return PDOStatement
     */
    function prepareRepositoryCreateStatement(): PDOStatement
    {
        $tableName = $this::TABLE_NAME;
        $index = $this::COLUMN_INDEX;
        $name = $this::COLUMN_NAME;
        $password = $this::COLUMN_PASSWORD;
        $firstName = $this::COLUMN_FIRST_NAME;
        $lastName = $this::COLUMN_LAST_NAME;
        $birthday = $this::COLUMN_BIRTHDAY;

        return $this->pdo->prepare(<<<END
            CREATE TABLE IF NOT EXISTS `$tableName` (
                `$index` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
                `$name` varchar(30) NULL UNIQUE,
                `$password` CHAR(60) NULL,
                `$firstName` TINYTEXT NULL,
                `$lastName` TINYTEXT NULL,
                `$birthday` DATE NULL,
                PRIMARY KEY (`{$index}`)
            )DEFAULT CHARSET=utf8;
        END
        );
    }

    /**
     * @return PDOStatement
     */
    function prepareInsertUserStatement(): PDOStatement
    {
        $tableName = $this::TABLE_NAME;

        return $this->pdo->prepare(<<<END
            INSERT INTO `$tableName` () VALUES ();
        END
        );
    }

    /**
     * @param User $user
     *
     * @return PDOStatement
     */
    function prepareUpdateUserStatement(User $user): PDOStatement
    {
        $tableName = $this::TABLE_NAME;
        $index = $this::COLUMN_INDEX;

        return $this->pdo->prepare(<<<END
            UPDATE {$tableName} SET {$this->convertToUpdateString($this->returnMutations($user))} WHERE `{$index}` = {$user->getIndex()->getIndexValue()};
        END
        );
    }

    /**
     * @param User $user
     *
     * @return array
     */
    function returnMutations(User $user): array
    {
        return array_merge($user->getPendingRemoves(), $user->getPendingAdditions());
    }

//    /**
//     * @param User $user
//     *
//     * @return PDOStatement
//     */
//    function deleteUserStatement(User $user): PDOStatement
//    {
//        $tableName = $this::TABLE_NAME;
//        $index = $this::COLUMN_INDEX;
//
//        return $this->pdo->prepare(<<<END
//            DELETE FROM `{$tableName}` WHERE `{$index}` = {$user->getIndex()->getIndexValue()};
//        END
//        );
//    }

    /**
     * Removes records wit provided indexes.
     * @param array $indexLIst
     *
     * @return PDOStatement
     */
    function deleteStatement(array $indexLIst): PDOStatement{

        $tableName = $this::TABLE_NAME;
        $index = $this::COLUMN_INDEX;
        $inddeString = implode(',', $indexLIst);

        return $this->pdo->prepare(<<<END
            DELETE FROM `{$tableName}` WHERE `{$index}` IN ({$inddeString});
        END);
    }

    /**
     * @param EntryIndex $startIndex
     * @param int        $limit
     *
     * @return PDOStatement
     */
    function prepareGetUserStatement(EntryIndex $startIndex, int $limit = -1): PDOStatement
    {
        $tableName = $this::TABLE_NAME;
        $index = $this::COLUMN_INDEX;
        //todo: create statement once and only replace placeholders
        return $this->pdo->prepare(<<<END
            SELECT * FROM `{$tableName}` WHERE `{$index}` >= {$startIndex->getIndexValue()} {$this->returnIndexLimitStatement($limit)};
        END
        );
    }

    /**
     * @param EntryIndex $index
     *
     * @return int
     */
    protected function sanitizeStartIndex(EntryIndex $index): int
    {
        return $index->isDefault() ? 0 : $index->getIndexValue();
    }

    /**
     * @param int $limit
     *
     * @return string
     */
    protected function returnIndexLimitStatement(int $limit): string
    {
        return $limit > 0 ? "LIMIT $limit" : '';
    }

    /**
     * @param EntryIndex $index
     *
     * @return PDOStatement
     */
    function selectEntryStmt(EntryIndex $index): PDOStatement
    {
        $tableName = $this::TABLE_NAME;
        $indexName = $this::COLUMN_INDEX;
        return $this->pdo->prepare(<<<END
            SELECT * FROM `$tableName` WHERE `$indexName` = {$index->getIndexValue()};
        END
        );
    }

    /**
     * @param string $nameLabel
     *
     * @return PDOStatement
     */
    function getFindUserWithNameStatement(string $nameLabel): PDOStatement
    {
        $tableName = $this::TABLE_NAME;
        $index = $this::COLUMN_INDEX;
        $nameColumn = $this::COLUMN_NAME;

        return $this->pdo->prepare(<<<END
            SELECT `$index` FROM `$tableName` WHERE `$nameColumn` = $nameLabel;
        END
        );
    }

    /**
     * @return PDOStatement
     */
    function getLoadUserListStatement(): PDOStatement
    {
        $tableName = $this::TABLE_NAME;
        $index = $this::COLUMN_INDEX;
        $nameColumn = $this::COLUMN_NAME;

        return $this->pdo->prepare(<<<END
           SELECT `$index`, `$nameColumn` FROM `$tableName` WHERE `$nameColumn` IS NOT NULL AND `$nameColumn` != '' ORDER BY `$index` DESC;
        END
        );
    }

    function entryExistStmt(EntryIndex $index): PDOStatement{
        $tableName = $this::TABLE_NAME;
        $indexColumn = $this::COLUMN_INDEX;

        return $this->pdo->prepare(<<<END
            SELECT COUNT(DISTINCT `$indexColumn`) FROM `$tableName` WHERE `$indexColumn` = {$index->getIndexValue()}; 
        END);
    }
}