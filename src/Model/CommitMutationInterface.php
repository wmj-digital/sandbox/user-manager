<?php



namespace Wmj\UserManager\Model;


interface CommitMutationInterface
{
    function hasPendingAdditions(): bool;

    function getPendingAdditions(): array;

    function hasPendingRemoves(): bool;

    function getPendingRemoves(): array;

    function commitChanges(): void;

    function rejectChanges(): void;
}