<?php



namespace Wmj\UserManager\Model;


trait ArrayHasDuplicatesTrait
{
    /**
     * @param array $target
     *
     * @return bool
     */
    protected function arrayHasDuplicates(array $target):bool{
        return  count($target) !== count( array_unique($target));
    }
}