<?php


namespace Wmj\UserManager\Model\UserGroup;


use Wmj\UserManager\Model\CommitMutationInterface;
use Wmj\UserManager\Model\CommitMutationTratit;
use Wmj\UserManager\Model\User\User;

class UserList implements CommitMutationInterface
{
    use CommitMutationTratit;

    const SELF_NAME = 'user_list';

    function import(array $users):self{
        // todo: import from DTO object

        array_walk($users, fn(array $user) => array_push($this->members, $user['user_index']));
        return $this;
    }

    /**
     * Add users to group.
     *
     * @param int ...$userIndex
     */
    function addUser(int ...$userIndex): void
    {
        array_walk($userIndex, fn(int $user) => $this->scheduleToAdd($user, $user));
    }

    /**
     * Add users if not in group or remove if in group.
     *
     * @param int ...$users
     */
    function setUsers(int ...$users):void{
        $toAdd =array_diff($users, $this->members);
        $toRemove =  array_diff($this->members, $users);

        if(!empty($toAdd)) $this->addUser(...$toAdd);
        if(!empty($toRemove)) $this->removeUser(...$toRemove);
    }

    function removeUser(int ...$users): void
    {
        array_walk($users, fn(int $user) => $this->scheduleToRemove($user));
    }

    function getAllUser(): array
    {
        return $this->members;
    }

//    function getUser(User $user): User
//    {
//        return $this->members[$user->getIndex()->value()];
//    }

    function haveUser(User $user): bool
    {
        return in_array($user, $this->members);
//        return array_key_exists($user->getIndex()->value(), $this->members);
    }
}