<?php


namespace Wmj\UserManager\Model\UserGroup;


use PDO;
use Wmj\UserManager\Model\EntryIndex;
use Wmj\UserManager\Model\StorageService;

/**
 * Class UserGroupReposiotry
 *
 * @package Wmj\UserManager\Model\UserGroup
 */
class UserGroupReposiotry
{
    protected PDO $pdo;
    /**
     * @var StorageService
     */
    private StorageService $storageService;
    /**
     * @var MySqlUserGroupStatementBuilder
     */
    private MySqlUserGroupStatementBuilder $statementBuilder;
    /**
     * @var UserGroupBuilder
     */
    private UserGroupBuilder $groupBuilder;

    /**
     * UserGroupReposiotry constructor.
     *
     * @param StorageService                 $storageProvider
     * @param MySqlUserGroupStatementBuilder $userGroupStorage
     * @param UserGroupBuilder               $groupBuilder
     */
    function __construct(StorageService $storageProvider, MySqlUserGroupStatementBuilder $userGroupStorage, UserGroupBuilder $groupBuilder)
    {
        $this->storageService = $storageProvider;
        $this->statementBuilder = $userGroupStorage;
        $this->groupBuilder = $groupBuilder;

        $this->createRepository();
    }

    /**
     *
     */
    private function createRepository(): void
    {
        $this->storageService->executeStatement($this->statementBuilder->returnGroupCreateStatement());
    }

    /**
     * @param UserGroup ...$groups
     */
    function save(UserGroup ...$groups): void
    {
        $this->failIfNameDuplicated(... $groups);
        $this->storageService->beginTransaction();

        foreach ($groups as $group) {
            if ($group->getIndex()->isDefault()) $this->insertGroup($group);
            if ($group->hasPendingChanges()) $this->updateGroup($group);
            if (($group->getUserList()->hasPendingChanges())) {
                $this->removeGroupMembers($group);
                $this->addGroupMembers($group);
                //todo: groupList should only accepts valid users
            }
        }

        $this->storageService->commitTransaction();
        array_walk($groups, function (UserGroup $userGroup): void {
            $userGroup->commitChanges();
            $userGroup->getUserList()->commitChanges();
        });
    }

//    function userGroupExist(UserGroupIndex $index): bool
//    {
//        return (bool)$this->storageService->executeStatement($this->statementBuilder->entryExistStmt($index)
//            ->fetchColumn());
//    }

    protected function failIfNameDuplicated(UserGroup ...$groups): void
    {
        //todo: refactor
//        $names = $this->returnPendingAdditions(...$users);
//        if ($this->arrayHasDuplicates($names)) throw new DuplicatedNameInTransactionGroupException();
//        if ($this->nameExistInDb($names)) throw new UserNameAlreadyExistException();
    }

    /**
     * @param UserGroup $group
     */
    protected function insertGroup(UserGroup $group): void
    {
        $statement = $this->statementBuilder->getAddGroupStatement();
        $this->storageService->executeStatement($statement);
        $this->groupBuilder->changeIndexValue($group, $this->storageService->getLastInsertedId());
    }

    /**
     * @param UserGroup $group
     */
    protected function updateGroup(UserGroup $group): void
    {
        $nameLabel = ':label';
        $statement = $this->statementBuilder->getUpdatGroupStatement($group, $nameLabel);
        //todo: refactor
        $mutation = $group->getPendingAdditions();
        $statement->bindValue($nameLabel, $mutation[UserGroup::NAME]);

        $this->storageService->executeStatement($statement);
    }

    /**
     * @param UserGroup $group
     */
    protected function removeGroupMembers(UserGroup $group): void
    {
        $memberIndexLabel = ':memberIndex';
        $storage = $this->storageService;
        $userToRemove = array_keys($group->getUserList()->getPendingRemoves());

        $statement = $this->statementBuilder->getRemoveGroupMemberStatement($group, $memberIndexLabel);
        array_walk($userToRemove, function (int $user) use ($statement, $memberIndexLabel, $storage) {
            $statement->bindValue($memberIndexLabel, $user, PDO::PARAM_INT);
            $storage->executeStatement($statement);
        });
    }

    /**
     * @param UserGroup $group
     */
    protected function addGroupMembers(UserGroup $group): void
    {
        $memberIndexLabel = ':memberIndex';
        $storage = $this->storageService;
        $userToAdd = $group->getUserList()->getPendingAdditions();


        $statement = $this->statementBuilder->getAddGroupMembersStatement($group, $memberIndexLabel);
        array_walk($userToAdd, function (int $user) use ($statement, $memberIndexLabel, $storage) {
            $statement->bindValue($memberIndexLabel, $user);
            $storage->executeStatement($statement);
        });
    }

    /**
     * @param UserGroupIndex $groupIndex
     *
     * @return UserGroup
     */
    function loadUserGroup(UserGroupIndex $groupIndex): UserGroup
    {
        $details = $this->loadGroupDetails($groupIndex);
        $group = $this->groupBuilder->createUserGroup();
        $group->setIndex($groupIndex);
        $group->import($details);
        $group->getUserList()->import($this->getGroupMember($groupIndex));
        return $group;
        //todo: load members index
    }

    /**
     * @param EntryIndex $groupIndex
     *
     * @return array
     */
    function loadGroupDetails(EntryIndex $groupIndex): array
    {
        $statement = $this->statementBuilder->getSelectGroupDetailsStatement($groupIndex);
        $this->storageService->executeStatement($statement);
        $groupDetals = ($statement->fetch(PDO::FETCH_ASSOC));
        return $groupDetals === false ? [] : $groupDetals;
    }

    /**
     * @param UserGroup ...$groups
     */
    function deleteGroups(UserGroup ...$groups): void
    {
        $this->delete(...array_map(fn(UserGroup $userGroup): EntryIndex => $userGroup->getIndex(), $groups));
    }

    /**
     * @param UserGroupIndex ...$groupIndices
     */
    function delete(UserGroupIndex ...$groupIndices):void{
        //todo code is redundant in user repository, move its to abstract class or trait
        $this->storageService->beginTransaction();

        $this->storageService->executeStatement(
            $this->statementBuilder->deleteStatement(array_map(fn(UserGroupIndex $index): int => $index->getIndexValue(), $groupIndices))
        );

        $this->storageService->commitTransaction();
        array_walk($userIndices, fn(UserGroupIndex $index) => $index->reset());
    }

    /**
     * @return array
     */
    function getGroupList(): array
    {
        //todo add filtering EntryIndex $startId = null, int $imit = -1 | and include exclude specyfic index
        $statement = $this->statementBuilder->getSelectAllGroupStatement();

        return $this->storageService->executeStatement($statement)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param EntryIndex $userIndex
     *
     * @return int[]
     */
    function getGroupsWithUser(EntryIndex $userIndex): array
    {
        $statement = $this->statementBuilder->getSelectUserGroupsStatement($userIndex);

        return $this->storageService->executeStatement($statement)->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * @param EntryIndex $groupIndex
     *
     * @return int[]
     */
    function getGroupMember(EntryIndex $groupIndex): array
    {
        $statement = $this->statementBuilder->getSelectGroupMembersStatement($groupIndex);

        return $this->storageService->executeStatement($statement)->fetchAll(PDO::FETCH_ASSOC);
    }

    function entryExist(UserGroupIndex $index): bool
    {
        return (bool)$this->storageService
            ->executeStatement($this->statementBuilder->entryExistStmt($index))
            ->fetchColumn();
    }
}