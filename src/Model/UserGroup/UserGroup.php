<?php


namespace Wmj\UserManager\Model\UserGroup;


use Closure;
use Wmj\UserManager\Model\AbstractEntity;
use Wmj\UserManager\Model\UserGroup\Validation\GroupNameCannotBeEmpty;

/**
 * Class UserGroup
 *
 * @package Wmj\UserManager\Model\UserGroup
 */
class UserGroup extends AbstractEntity
{
    const COLLECTION_NAME = 'user_groups';
    const NAME = 'name';
    /**
     * @var UserGroupIndex
     */
    private UserGroupIndex $index;
    /**
     * @var UserList
     */
    private UserList $userList;

    /**
     * UserGroup constructor.
     *
     * @param UserGroupIndex $userGroupIndex
     * @param UserList       $userLIst
     */
    function __construct(UserGroupIndex $userGroupIndex, UserList $userLIst)
    {
        $this->index = $userGroupIndex;
        $this->userList = $userLIst;
    }


    /**
     * @return UserGroupIndex
     */
    function getIndex(): UserGroupIndex
    {
        return $this->index;
    }

    function setIndex(UserGroupIndex $index):self{
        $this->index= $index;
        return $this;
    }

    /**
     * @return UserList
     */
    function getUserList(): UserList
    {
        return $this->userList;
    }

    /**
     * @return Closure[]
     */
    protected function getSimpleValueNameProcessorPairs(): array
    {
        return [
            self::NAME => function (string $name): string {
                if (empty($name)) throw new GroupNameCannotBeEmpty();
                return htmlspecialchars($name); //todo: sanitization | escape illegal characters
            }
        ];
    }

    /**
     * @return array
     */
    protected function getComplexValues(): array
    {
        return [
            $this->index::INDEX_LABEL => $this->index,
            $this->userList::SELF_NAME => $this->userList
        ];
    }
}