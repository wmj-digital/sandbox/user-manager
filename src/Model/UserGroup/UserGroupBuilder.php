<?php


namespace Wmj\UserManager\Model\UserGroup;


use Wmj\UserManager\Model\EntryIndex;

/**
 * Class UserGroupBuilder
 *
 * @package Wmj\UserManager\Model\UserGroup
 */
class UserGroupBuilder
{
    /**
     * @var EntryIndex
     */
    private EntryIndex $indexProto;

    /**
     * UserGroupBuilder constructor.
     *
     * @param UserGroupIndex $indexPrototype
     */
    function __construct(UserGroupIndex $indexPrototype){
        $this->indexProto = $indexPrototype;
    }

    /**
     * @return UserGroupIndex
     */
    function getIndexProto():UserGroupIndex{
        return $this->indexProto;
    }

    /**
     * @return UserGroup
     */
    function createUserGroup():UserGroup{
        return new UserGroup($this->indexProto->cloneDefaultIndex() ,$this->createUserList());
    }

    /**
     * @param int $index
     *
     * @return UserGroup
     */
    function restoreUserGroup(int $index):UserGroup{
        return new UserGroup($this->indexProto->returnEntryIndex($index), $this->createUserList());
    }

    /**
     * @return UserList
     */
    protected function createUserList():UserList{
        return new UserList();
    }

    /**
     * @param UserGroup $user
     * @param int       $newIndexValue
     */
    function changeIndexValue(UserGroup $user, int $newIndexValue):void{
        $user->setIndex($this->indexProto->returnEntryIndex($newIndexValue));
    }
}