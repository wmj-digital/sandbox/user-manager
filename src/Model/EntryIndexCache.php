<?php


namespace Wmj\UserManager\Model;


use RuntimeException;
use Wmj\UserManager\ObjectCache\ObjectWeakCacheTrait;


/**
 * Class EntryIndexCache
 *
 * @package Wmj\UserManager\Model
 */
class EntryIndexCache
{
    use ObjectWeakCacheTrait{
        addToCache as protected addToCacheBase;
        getFromCache as protected getFromCacheBase;
    }

    /**
     * @param EntryIndex $entryIndex
     *
     * @return EntryIndex
     */
    function addToCache(EntryIndex $entryIndex): EntryIndex
    {
        if ($entryIndex->isDefault()) throw new RuntimeException('Default index cannot be add to indexCache');
        $this->addToCacheBase($entryIndex);

        return $entryIndex;
    }

    /**
     * @param int $indexValue
     *
     * @return EntryIndex
     *
     * @noinspection PhpIncompatibleReturnTypeInspection
     */
    function getFromCache(int $indexValue): ?EntryIndex
    {
        return $this->getFromCacheBase($indexValue);
    }
}