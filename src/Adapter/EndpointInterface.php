<?php


namespace Wmj\UserManager\Adapter;

/**
 * Interface EndpointInterface
 *
 * @package Wmj\UserManager\Controller
 */
interface EndpointInterface
{
    /**
     * @param string $method
     * @param string $uri
     * @param array  $inputData
     */
    function handleRequest(string $method, string $uri, array $inputData): void;
}