<?php


namespace Wmj\UserManager\Adapter;




use Wmj\UserManager\Adapter\RestApi\UserGroups\GroupCollectionEndpoint;
use Wmj\UserManager\Adapter\RestApi\UserGroups\GroupEndpoint;
use Wmj\UserManager\Adapter\RestApi\UserGroups\GroupMembersEndpoint;
use Wmj\UserManager\Adapter\RestApi\Users\UserEndpoint;
use Wmj\UserManager\Adapter\RestApi\Users\UserGroupEndpoint;
use Wmj\UserManager\Adapter\RestApi\Users\UsersCollectionEndpoint;

class Router
{
    protected const REST_API_PREFIX = 'api/v1';
    protected const REGEX_DELIMITER = '|';

    /**
     * @param string $uri
     *
     * @return string
     */
    function findEndpointClass(string $uri): string
    {
        switch (true) {
            case $this->rootEndpointMatch($uri):
                return RootEndpoint::class;

            case $this->userCollectionEndpointMatch($uri):
                return UsersCollectionEndpoint::class;

            case $this->userEndpointMatch($uri):
                return UserEndpoint::class;

            case $this->groupsAssignedToUserEndpointMatch($uri):
                return UserGroupEndpoint::class;

            case $this->userGroupMembersEndpointMatch($uri):
                return GroupMembersEndpoint::class;

            case $this->userGroupCollectionEndpointMatch($uri):
                return GroupCollectionEndpoint::class;

            case $this->userGroupEndpointMatch($uri):
                return GroupEndpoint::class;

            default:
                return DefaultEndpoint::class;
        }
    }

    /**
     * @param $uri
     *
     * @return bool
     */
    protected function rootEndpointMatch($uri): bool
    {
        return $uri === '/' || $uri === '';
    }

    /**
     * @param string $uri
     *
     * @return bool
     */
    protected function userCollectionEndpointMatch(string $uri): bool
    {
        return preg_match($this->returnRestApiPattern('/users/?$'), $uri);
    }

    /**
     * @param string $uri
     *
     * @return bool
     */
    protected function userEndpointMatch(string $uri):bool{
        return preg_match($this->returnRestApiPattern('/users/?[0-9]*$'), $uri);
    }

    /**
     * @param string $uri
     *
     * @return bool
     */
    protected function groupsAssignedToUserEndpointMatch(string $uri):bool{
        return preg_match($this->returnRestApiPattern('/users/?[0-9]*/groups/?$'), $uri);
    }

    /**
     * @param string $route
     *
     * @return string
     */
    private function returnRestApiPattern(string $route): string
    {
        return self::REGEX_DELIMITER . self::REST_API_PREFIX . $route . self::REGEX_DELIMITER;
    }

    /**
     * @param string $uri
     *
     * @return bool
     */
    protected function userGroupEndpointMatch(string $uri): bool
    {
        return preg_match($this->returnRestApiPattern('/users-groups/[0-9]*$'), $uri);
    }

    /**
     * @param string $uri
     *
     * @return bool
     */
    protected function userGroupMembersEndpointMatch(string $uri): bool
    {
        return preg_match($this->returnRestApiPattern('/users-groups/[0-9]+/members/?$'), $uri);
    }

    /**
     * @param string $uri
     *
     * @return false|int
     */
    protected function userGroupCollectionEndpointMatch(string $uri)
    {
        return preg_match($this->returnRestApiPattern('/users-groups/?$'), $uri);
    }
}