<?php


namespace Wmj\UserManager\Adapter\RestApi\UserGroups;


use Exception;
use RuntimeException;
use Wmj\UserManager\Adapter\DefaultEndpoint;
use Wmj\UserManager\Model\UserGroup\UserGroupBuilder;
use Wmj\UserManager\Model\UserGroup\UserGroupReposiotry;
use Wmj\UserManager\Model\ValidationException;

/**
 * Class GroupEndpoint
 *
 * @package Wmj\UserManager\Adapter\RestApi\UserGroups
 */
class GroupEndpoint extends DefaultEndpoint
{
    /**
     * @var UserGroupReposiotry
     */
    protected UserGroupReposiotry $groupRepository;
    /**
     * @var UserGroupBuilder
     */
    protected UserGroupBuilder $groupBuilder;

    /**
     * GroupEndpoint constructor.
     *
     * @param UserGroupReposiotry $groupRepository
     * @param UserGroupBuilder    $groupBuilder
     */
    function __construct(UserGroupReposiotry $groupRepository, UserGroupBuilder $groupBuilder)
    {
        $this->groupRepository = $groupRepository;
        $this->groupBuilder = $groupBuilder;
    }

    /**
     * @inheritDoc
     */
    function handleRequest(string $method, string $uri, array $inputData): void
    {
        switch ($method) {
            case (self::GET):
                $this->sendGroupDetails($this->getLastUriItem($uri));
                break;
            case (self::POST):
                $this->updateGroup($this->getLastUriItem($uri), $inputData);
                break;
            case (self::DELETE):
                $this->deleteGroup($this->getLastUriItem($uri));
                break;
            default:
                parent::handleRequest($method, $uri, $inputData);
        }
    }

    /**
     * @param int $groupIndex
     */
    protected function sendGroupDetails(int $groupIndex): void
    {
        $userDetails = $this->groupRepository->loadGroupDetails($this->groupBuilder->getIndexProto()->returnEntryIndex($groupIndex));

        if (empty($userDetails)) {
            $this->sendResponceCode(
                self::NOT_FOUND
            );
        } else {
            $this->sendJsonResponce(
                self::OK,
                json_encode($userDetails)
            );
        }
    }

    /**
     * @param int   $groupIndex
     * @param array $payload
     */
    protected function updateGroup(int $groupIndex, array $payload): void
    {
        try {
            $this->groupRepository->save($this->groupBuilder->restoreUserGroup($groupIndex)->set($payload));

            $this->sendResponceCode(self::OK);
        } catch (ValidationException $exception) {
            //todo: send validation exceptions as error message
            $this->sendResponceCode(
                self::BAD_REQUEST
            );
        } catch (Exception $exception) {
            $this->sendResponceCode(
                self::INTERNAL_SERVER_ERROR
            );

            throw new RuntimeException($exception);
        }
    }

    /**
     * @param int $groupIndex
     */
    protected function deleteGroup(int $groupIndex): void
    {
        try {
            $this->groupRepository->delete($this->groupBuilder->getIndexProto()->returnEntryIndex($groupIndex));

            $this->sendResponceCode(self::OK);
        } catch (Exception $exception) {
            $this->sendResponceCode(self::INTERNAL_SERVER_ERROR);
        }
    }
}