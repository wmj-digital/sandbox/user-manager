<?php


namespace Wmj\UserManager\Adapter\RestApi\UserGroups;


use Exception;
use RuntimeException;
use Wmj\UserManager\Adapter\DefaultEndpoint;
use Wmj\UserManager\Model\UserGroup\UserGroupBuilder;
use Wmj\UserManager\Model\UserGroup\UserGroupReposiotry;

/**
 * Class GroupMembersEndpoint
 *
 * @package Wmj\UserManager\Adapter\RestApi\UserGroups
 */
class GroupMembersEndpoint extends DefaultEndpoint
{
    /**
     * @var UserGroupReposiotry
     */
    protected UserGroupReposiotry $userGroupRepository;

    /**
     * @var UserGroupBuilder
     */
    protected UserGroupBuilder $groupBuilder;

    /**
     * GroupMembersEndpoint constructor.
     *
     * @param UserGroupReposiotry $userGroupReposiotry
     * @param UserGroupBuilder    $groupBuilder
     */
    function __construct(UserGroupReposiotry $userGroupReposiotry, UserGroupBuilder $groupBuilder)
    {
        $this->userGroupRepository = $userGroupReposiotry;
        $this->groupBuilder = $groupBuilder;
    }

    /**
     * @inheritdoc
     **/
    function handleRequest(string $method, string $uri, array $inputData): void
    {
        switch ($method) {
            case (self::GET):
                $this->sendGroupMembers($this->getOneFeforeLastUriItem($uri));
                break;
            case (self::POST):
                $this->setGroupMembers($this->getOneFeforeLastUriItem($uri), $inputData);
                break;
            default:
                parent::handleRequest($method, $uri, $inputData);
        }

    }

    /**
     * @param int $groupId
     */
    protected function sendGroupMembers(int $groupId): void
    {
        try {
            $this->sendJsonResponce(
                self::OK,
                json_encode($this->userGroupRepository->getGroupMember($this->groupBuilder->getIndexProto()->returnEntryIndex($groupId)))
            );
        } catch (Exception $exception) {
            $this->sendResponceCode(
                self::INTERNAL_SERVER_ERROR
            );

            throw new RuntimeException($exception);
        }
    }

    /**
     * @param int   $groupId
     * @param array $payload
     */
    protected function setGroupMembers(int $groupId, array $payload): void
    {
        $membersLabel = 'members';

        try {
            if (!isset($payload[$membersLabel]) || empty($payload[$membersLabel])) {
                $this->sendResponceCode(
                    self::NO_CONTENT
                );
            } else {
                $group = $this->userGroupRepository->loadUserGroup(
                    $this->groupBuilder->getIndexProto()->returnEntryIndex($groupId)
                );
                $group->getUserList()->setUsers(...$payload[$membersLabel]);
                $this->userGroupRepository->save($group);

                $this->sendResponceCode(self::OK);
            }
        } catch (Exception $exception) {
            $this->sendResponceCode(
                self::INTERNAL_SERVER_ERROR
            );

            throw new RuntimeException($exception);
        }

    }
}