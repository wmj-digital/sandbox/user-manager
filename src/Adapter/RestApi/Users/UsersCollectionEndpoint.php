<?php


namespace Wmj\UserManager\Adapter\RestApi\Users;


use Exception;
use RuntimeException;
use Wmj\UserManager\Adapter\DefaultEndpoint;
use Wmj\UserManager\Model\User\User;
use Wmj\UserManager\Model\User\UserBuilder;
use Wmj\UserManager\Model\User\UserIndex;
use Wmj\UserManager\Model\User\UserRepository;
use Wmj\UserManager\Model\ValidationException;

/**
 * Class UsersCollectionEndpoint
 *
 * @package Wmj\UserManager\Adapter\RestApi\Users
 */
class UsersCollectionEndpoint extends DefaultEndpoint
{
    const PATH = 'api/v1/users';
    /**
     * @var UserBuilder
     */
    protected UserBuilder $userBuilder;
    /**
     * @var UserRepository
     */
    protected UserRepository $userRepository;

    /**
     * UsersCollectionEndpoint constructor.
     *
     * @param UserBuilder    $userBuilder
     * @param UserRepository $userRepository
     */
    function __construct(UserBuilder $userBuilder, UserRepository $userRepository)
    {
        $this->userBuilder = $userBuilder;
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritDoc
     */
    function handleRequest(string $method, string $uri, array $inputData): void
    {
        switch ($method) {
            case self::GET:
                $this->sendUsersList();
                break;
            case self::POST:
                $this->addUserToCollection($inputData);
                break;
            default:
                parent::handleRequest($method, $uri, $inputData);
        }
    }

    /**
     *
     */
    protected function sendUsersList(): void
    {
        try {
            $this->sendJsonResponce(
                self::OK,
                json_encode($this->userRepository->loadUserList())
            );
        } catch (Exception $exception) {
            $this->sendResponceCode(
                self::INTERNAL_SERVER_ERROR
            );
        }
    }

    /**
     * @param array $payload
     */
    private function addUserToCollection(array $payload): void
    {
        try {
            $user = $this->userBuilder->createUser()->set($payload);
            $this->userRepository->save($user);

            $this->sendJsonResponce(
                self::CREATED,
                json_encode([
                    UserIndex::INDEX_LABEL => $user->getIndex()->getIndexValue(),
                    User::NAME => $user->offsetGet(User::NAME)
                ])
            );
        } catch (ValidationException $exception) {
            $this->sendResponceCode(
                self::BAD_REQUEST
            );
        } catch (Exception $exception) {
            $this->sendResponceCode(
                self::INTERNAL_SERVER_ERROR
            );

            throw new RuntimeException($exception->getMessage());
        }
    }
}