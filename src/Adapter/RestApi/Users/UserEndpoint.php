<?php


namespace Wmj\UserManager\Adapter\RestApi\Users;


use Exception;
use RuntimeException;
use Throwable;
use Wmj\UserManager\Adapter\DefaultEndpoint;
use Wmj\UserManager\Model\ArrayHasDuplicatesTrait;
use Wmj\UserManager\Model\EntryNotExistInStorageException;
use Wmj\UserManager\Model\ReturnPendingAdditionsTrait;
use Wmj\UserManager\Model\User\User;
use Wmj\UserManager\Model\User\UserBuilder;
use Wmj\UserManager\Model\User\UserRepository;
use Wmj\UserManager\Model\ValidationException;

/**
 * Class UserEndpoint
 *
 * @package Wmj\UserManager\Adapter\RestApi\Users
 */
class UserEndpoint extends DefaultEndpoint
{
    use ReturnPendingAdditionsTrait;
    use ArrayHasDuplicatesTrait;

    /**
     * @var UserBuilder
     */
    private UserBuilder $userBuilder;
    /**
     * @var UserRepository
     */
    private UserRepository $userRepository;

    /**
     * UserEndpoint constructor.
     *
     * @param UserBuilder    $userBuilder
     * @param UserRepository $userRepository
     */
    function __construct(UserBuilder $userBuilder, UserRepository $userRepository)
    {
        $this->userBuilder = $userBuilder;
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritDoc
     */
    function handleRequest(string $method, string $uri, array $inputData): void
    {
        switch ($method) {
            case self::GET:
                $this->sendUserDetails($this->getLastUriItem($uri));
                break;
            case self::POST: //todo: find solution to reive data from put method
                $this->updateUser($this->getLastUriItem($uri), $inputData);
                break;
            case self::DELETE:
                $this->deleteUser($this->getLastUriItem($uri));
                break;
            default:
                parent::handleRequest($method, $uri, $inputData);
        }

    }

    /**
     * @param int $userIndex
     */
    protected function sendUserDetails(int $userIndex): void
    {
        try {
            $userDetails = $this->userRepository
                ->loadEntryDetails($this->userBuilder->getIndexProto()->returnEntryIndex($userIndex));

            unset($userDetails[User::PASSWORD]);
            $this->sendJsonResponce(self::OK, json_encode($userDetails));
        } catch (EntryNotExistInStorageException $exception) {
            $this->sendResponceCode(self::NOT_FOUND);
        } catch (ValidationException $exception) {
            $this->sendResponceCode(self::BAD_REQUEST);
        } catch (Throwable $throwable) {
            $this->sendResponceCode(self::INTERNAL_SERVER_ERROR);
            throw new RuntimeException(get_class($this), 0, $throwable);
        }
    }

    /**
     * @param int   $userIndex
     * @param array $payload
     */
    protected function updateUser(int $userIndex, array $payload)
    {

        try {
            $this->userRepository
                ->save($this->userBuilder->restoreUser($userIndex)->set($payload));

            $this->sendResponceCode(self::OK);
        } catch (EntryNotExistInStorageException $exception) {
            $this->sendResponceCode(self::NOT_FOUND);
        } catch (ValidationException $exception) {
            //todo: send validation exceptions as error message
            $this->sendResponceCode(self::BAD_REQUEST);
        } catch (Exception $exception) {
            $this->sendResponceCode(self::INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @param int $userIndex
     */
    protected function deleteUser(int $userIndex): void
    {
        try {
            $this->userRepository
                ->delete($this->userBuilder->getIndexProto()->returnEntryIndex($userIndex));

            $this->sendResponceCode(self::OK);
        } catch (EntryNotExistInStorageException $exception) {
            $this->sendResponceCode(self::NOT_FOUND);
        } catch (ValidationException $exception) {
            //todo: send validation exceptions as error message
            $this->sendResponceCode(self::BAD_REQUEST);
        } catch (Exception $exception) {
            $this->sendResponceCode(self::INTERNAL_SERVER_ERROR);
        }
    }
}