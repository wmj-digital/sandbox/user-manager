<?php


namespace Wmj\UserManager\Adapter;


class RootEndpoint extends DefaultEndpoint
{

    function handleRequest(string $method, string $uri, array $inputData): void
    {
        if ($method === self::GET) $this->sendAppUi();
        else {
            parent::handleRequest($method, $uri, $inputData);
        }
    }

    protected function sendAppUi(): void
    {
        echo file_get_contents('./public/app.html');
    }
}