<?php


namespace Wmj\UserManager;


use RuntimeException;

/**
 * Trait PrototypeTrait
 *
 * @package Wmj\UserManager
 */
trait PrototypeTrait
{
    protected bool $isPrototype = false;

    /**
     * @return $this
     */
    function markAsPrototype(): self
    {
        $this->isPrototype = true;
        return $this;
    }

    /**
     * @return bool
     */
    function isPrototype(): bool
    {
        return $this->isPrototype;
    }

    /**
     *
     */
    private function __clone()
    {
        $this->isPrototype = false;
    }
}