<?php


namespace Wmj\UserManager\ObjectCache;


use WeakReference;

trait ObjectWeakCacheTrait
{
    use ObjectCacheIdValidationTrait;

    /**
     * @var WeakReference[]
     */
    protected array $cache = [];

    /**
     * @param object $subject
     * @param null   $id
     */
    function addToCache(object $subject, $id = null): object
    {
        $this->validateIdOrFail($id);
        $this->cache[$id ?? get_class($subject)] = WeakReference::create($subject);

        return $subject;
    }

    /**
     * @param $id
     *
     * @return object
     */
    function getFromCache($id): ?object
    {
        return $this->inCache($id) ? $this->cache[$id]->get() : null;
    }

    /**
     * @param $id
     *
     * @return bool
     */
    function inCache($id): bool
    {
        return array_key_exists($id, $this->cache);
    }
}