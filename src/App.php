<?php



namespace Wmj\UserManager;


use Throwable;
use Wmj\UserManager\Assets\Configuration;
use Wmj\UserManager\Adapter\EndpointInterface;
use Wmj\UserManager\Adapter\Router;

/**
 * Class App
 *
 * @package Wmj\UserManager
 */
class App
{
    /**
     * @var Configuration
     */
    private Configuration $configuration;
    /**
     * @var DependencyContainer
     */
    private DependencyContainer $dc;

    /**
     * App constructor.
     */
    private function __construct()
    {
        $this->loadConfiguration();
        $this->loadDependencyContainer();
    }

    /**
     *
     */
    private function loadConfiguration(): void
    {
        $this->configuration = new Configuration();
    }

    /**
     *
     */
    private function loadDependencyContainer(): void
    {
        $this->dc = new DependencyContainer($this->configuration);
    }

    /**
     * @return static
     */
    static function instance(): self
    {
        return new self();
    }

    /**
     *
     */
    function run(): void
    {
        try {
            $this->getEndpoint(
                $this->getRouter()->findEndpointClass($this->getRequestUri())
            )->handleRequest($this->getRequestMethod(), $this->getRequestUri(), $this->getRequestInput());
        } catch (Throwable $throwable) {
            var_dump($throwable); //todo: put to log file
        }
    }

    /**
     * @param string $endpointClass
     *
     * @return EndpointInterface
     */
    private function getEndpoint(string $endpointClass): EndpointInterface
    {
        return $this->dc->get($endpointClass);
    }

    /**
     * @return Router
     */
    private function getRouter(): Router
    {
        return $this->dc->get(Router::class);
    }

    /**
     * @return string
     */
    private function getRequestUri(): string
    {
        return $_SERVER['REQUEST_URI'];
    }

    /**
     * @return string
     */
    private function getRequestMethod(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    /**
     * @return array
     */
    private function getRequestInput(): array
    {
        $input = array_merge($_GET, $_POST);
        unset($input['path']);
        return $input;
    }
}
