const Home = {
    template: `
        <h1></h1>
    `,
    data() {
        return {}
    },
    methods: {}
}

Vue.component('user-list', UserList)
Vue.component(VueRouter)
const routes = [
    {
        path: '',
        name: 'Home',
        component: Home
    },
    {
        path: '/user-list',
        name: 'User list',
        component: UserList
    },
    {
        path: '/user-details/:index',
        name: 'User details',
        component: UserDetails
    },
    {
        path: '/user-edit/:index',
        name: 'User editor',
        component: UserEdit
    },
    {
        path: '/user-add',
        name: 'User create',
        component: UserCreate
    },
    {
        path: '/group-list',
        name: 'Group',
        component: GroupList
    },
    {
        path: '/group-details/:index',
        name: 'Groups details',
        component: GroupDetails
    },
    {
        path: '/group-create',
        name: 'Group create',
        component: GroupCreate
    },
    {
        path: '/group-edit/:index',
        name: 'Group details editor',
        component: GroupEdit
    },
    {
        path: '/group-members-edit/:index',
        name: 'Group members editor',
        component: GroupMembersEdit
    }
]


const router = new VueRouter({
    // mode: 'history',
    base: location.pathname.substring(1),
    routes
})


const app = new Vue({
    router,
}).$mount('#app')